/**
 * Signee Sample App
 * Author: Huynh Duc Hau
 * Copyright ElinkGate JSC 2019
 */
package com.example.signeelauncher;

import android.app.admin.DeviceAdminReceiver;
import android.content.ComponentName;
import android.content.Context;

public class MyDeviceAdminReceiver extends DeviceAdminReceiver {
    public ComponentName getComponentName(Context context) {
        return new ComponentName(context.getApplicationContext(), this.getClass());
    }
}
